package com.example.volod.calculator;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

class CalculatorView {

    private TextView textDown, textUp;

    CalculatorView(View rootView) {
        textUp = (TextView) rootView.findViewById(R.id.textUp);
        textDown = (TextView) rootView.findViewById(R.id.textDown);
    }

    public void showDownInfo(String string) {
        textDown.setText(string);
    }

    public void showUpInfo(String string) {
        textUp.setText(string);
    }
}

class ConverterView {
    private Button btCurs;
    Float result;

    ConverterView(View rootView) {
        btCurs = rootView.findViewById(R.id.buttonCurs);
    }

    public void setResult(Float result) {
        btCurs.setText(String.valueOf(result));
    }
}
