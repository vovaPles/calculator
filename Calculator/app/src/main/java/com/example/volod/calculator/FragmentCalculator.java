package com.example.volod.calculator;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentCalculator extends android.app.Fragment implements View.OnClickListener {

    CalculatorView calculatorView;
    Button btQuad ,btPlusMinus, btZero, btOne, btTwo, btThree, btFour, btFive, btSix, btSeven,
            btEight, btNine, btBackspace, btCe, btC, btEquals, btMultiplication,
            btDivision, btSubtraction, btAddition;
    int MULTIPLICATION = 1;
    int DIVISION = 2;
    int ADDITION = 3;
    int SUBTRACTION = 4;

    CalculatorModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_calculator, null);
        model = new CalculatorModel();
        calculatorView = new CalculatorView(view);

        if (savedInstanceState != null) {
            model.setDownText(savedInstanceState.getString("down"));
            model.setUpText(savedInstanceState.getString("up"));
            model.setOperand1(savedInstanceState.getDouble("op1"));
            model.setOperand2(savedInstanceState.getDouble("op2"));
            model.setAction(savedInstanceState.getInt("action"));
            showText();
        }

        btQuad = view.findViewById(R.id.quad);
        btZero = view.findViewById(R.id.zero);
        btPlusMinus = view.findViewById(R.id.plus_minus);
        btOne = view.findViewById(R.id.one);
        btTwo = view.findViewById(R.id.two);
        btThree = view.findViewById(R.id.three);
        btFour = view.findViewById(R.id.four);
        btFive = view.findViewById(R.id.five);
        btSix = view.findViewById(R.id.six);
        btSeven = view.findViewById(R.id.seven);
        btEight = view.findViewById(R.id.eight);
        btNine = view.findViewById(R.id.nine);
        btEquals = view.findViewById(R.id.result);
        btBackspace = view.findViewById(R.id.back_space);
        btCe = view.findViewById(R.id.ce);
        btC = view.findViewById(R.id.clean);
        btMultiplication = view.findViewById(R.id.multiplication);
        btDivision = view.findViewById(R.id.division);
        btAddition = view.findViewById(R.id.addition);
        btSubtraction = view.findViewById(R.id.subtraction);

        btQuad.setOnClickListener(this);
        btZero.setOnClickListener(this);
        btPlusMinus.setOnClickListener(this);
        btOne.setOnClickListener(this);
        btTwo.setOnClickListener(this);
        btThree.setOnClickListener(this);
        btFour.setOnClickListener(this);
        btFive.setOnClickListener(this);
        btSix.setOnClickListener(this);
        btSeven.setOnClickListener(this);
        btEight.setOnClickListener(this);
        btEquals.setOnClickListener(this);
        btNine.setOnClickListener(this);
        btC.setOnClickListener(this);
        btCe.setOnClickListener(this);
        btBackspace.setOnClickListener(this);
        btMultiplication.setOnClickListener(this);
        btDivision.setOnClickListener(this);
        btAddition.setOnClickListener(this);
        btSubtraction.setOnClickListener(this);
        showText();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.one:
                clickNumber(1);
                break;
            case R.id.two:
                clickNumber(2);
                break;
            case R.id.three:
                clickNumber(3);
                break;
            case R.id.four:
                clickNumber(4);
                break;
            case R.id.five:
                clickNumber(5);
                break;
            case R.id.six:
                clickNumber(6);
                break;
            case R.id.seven:
                clickNumber(7);
                break;
            case R.id.eight:
                clickNumber(8);
                break;
            case R.id.nine:
                clickNumber(9);
                break;
            case R.id.zero:
                clickNumber(0);
                break;
            case R.id.result:
                model.result();
                showText();
                break;
            case R.id.multiplication:
                model.changeOperand((String) btMultiplication.getText(), MULTIPLICATION);
                //showText();
                break;
            case R.id.division:
                model.changeOperand((String) btDivision.getText(), DIVISION);
                break;
            case R.id.back_space:
                model.backspace();
                showText();
                break;
            case R.id.addition:
                model.changeOperand((String) btAddition.getText(), ADDITION);
                break;
            case R.id.subtraction:
                model.changeOperand((String) btSubtraction.getText(), SUBTRACTION);
                break;
            case R.id.ce:
                model.cleanDown();
                showText();
                break;
            case R.id.clean:
                model.cleanAll();
                showText();
                break;
            case R.id.plus_minus:
                model.makeNegative();
                showText();
                break;
            case R.id.quad:
                model.toQuad();
                showText();
                break;
        }
        showText();
    }

    public void clickNumber(int number) {
        model.setOperand(number);
        showText();
    }

    public void showText() {
        calculatorView.showDownInfo(model.getDownText());
        calculatorView.showUpInfo(model.getUpText());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("down", model.getDownText());
        outState.putString("up", model.getUpText());
        outState.putDouble("op1", model.getOperand1());
        outState.putDouble("op2", model.getOperand2());
        outState.putInt("action", model.getAction());
    }
}