package com.example.volod.calculator;

import java.util.Stack;

public class CalculatorModel {

    private String s, downText, upText;
    private double res, operand1 = 0, operand2 = 0;
    private Stack<Integer> forBack = new Stack<>();
    private int action;
    final int MULTIPLICATION = 1;
    final int DIVISION = 2;
    final int ADDITION = 3;
    final int SUBTRACTION = 4;

    void toQuad() {
        if (getOperand1() != 0) {
            setOperand1(getOperand1() * getOperand1());
            if (getOperand1() % 1 == 0) setDownText(String.valueOf((int) getOperand1()));
            else setDownText(String.valueOf(getOperand1()));
        }
    }

    void setOperand(int downOperand) {

        forBack.push(downOperand);
        operand1 = operand1 * 10 + downOperand;
        if (operand1 % 1 == 0) {
            setDownText(String.valueOf((int) operand1));
        }
    }

    void backspace() {
        operand1 = (operand1 - forBack.get(forBack.size() - 1)) / 10;
        setDownText(String.valueOf((int) operand1));
    }

    void changeOperand(String string, int action) {
        boolean bool = getOperand1() != 0 || getOperand2() != 0 || !getDownText().equals("") || !getUpText().equals("");
        if (bool) {
            if (getOperand1() != 0 && getOperand2() != 0) {
                result();
            }

            this.action = action;
            if (!getDownText().equals("")) {
                operand2 = operand1;
                s = getDownText();
            }
            if (getOperand1() == 0) {
                setUpText(s + " " + string);
            } else setUpText(getDownText() + " " + string);
            cleanDown();

        }
    }

    void cleanDown() {
        setDownText("");
        operand1 = 0;
    }

    void cleanAll() {
        setDownText("");
        setUpText("");
        operand1 = 0;
        operand2 = 0;
        s = "";
        res = 0;
    }

    void result() {
        switch (action) {
            case MULTIPLICATION:
                res = operand2 * operand1;
                break;
            case DIVISION:
                res = operand2 / operand1;
                break;
            case ADDITION:
                res = operand2 + operand1;
                break;
            case SUBTRACTION:
                res = operand2 - operand1;
                break;
        }
        if (res % 1 == 0) setDownText(String.valueOf((int) res));
        else setDownText(String.valueOf(res));
        operand1 = res;
        operand2 = 0;
        setUpText("");
        action = 0;
    }

    void makeNegative() {
        operand1 *= -1;
        if (operand1 % 1 == 0)
            setDownText(String.valueOf((int) operand1));
        setDownText(String.valueOf(operand1));
    }

    void setDownText(String downText) {
        this.downText = downText;
    }

    void setUpText(String upText) {
        this.upText = upText;
    }

    String getDownText() {
        return downText;
    }

    String getUpText() {
        return upText;
    }

    void setOperand1(double operand1) {
        this.operand1 = operand1;
    }

    void setOperand2(double operand2) {
        this.operand2 = operand2;
    }

    double getOperand1() {
        return operand1;
    }

    double getOperand2() {
        return operand2;
    }

    void setAction(int action) {
        this.action = action;
    }

    int getAction() {
        return action;
    }
}

class ConverterModel {

    private Float result;

    ConverterModel(Float sum, Float curs) {
        if (sum != null && curs != null) {
            result = sum * curs;
        }else result = (float) 0;
    }

    public Float getResult() {return result;}
}
