package com.example.volod.calculator;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FragmentConverter extends Fragment {

    ConverterView converterView;
    Float curs = null, sum = null;
    EditText editCurs, editSum;
    ConverterModel model;
    Button buttonCurs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_converter, null);

        this.setRetainInstance(true);
        converterView = new ConverterView(view);


        editSum = view.findViewById(R.id.editSum);
        editCurs = view.findViewById(R.id.editCurs);

        buttonCurs = view.findViewById(R.id.buttonCurs);

        buttonCurs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //curs = Float.parseFloat(String.valueOf(editCurs.getText()));
                //sum = Float.parseFloat(String.valueOf(editSum.getText()));

                model = new ConverterModel(sum, curs);
                converterView.setResult(model.getResult());
            }
        });

        return view;
    }
}
